"""Module for sending target goals to the turtle_control node."""

from threading import Event
import rclpy
from rclpy.node import Node
from rclpy.action import ActionClient

from turtle_core.action import Target


class TurtlePlanner(Node):
    """Node for sending target goals to the turtle_control node."""

    def __init__(self):
        """Initialize the node."""
        super().__init__("turtle_planner")
        self.action_done_event = Event()
        self.action_client = ActionClient(self, Target, "target")
        self.action_client.wait_for_server()

    def send_goal(self, x: float, y: float):
        """Send a target goal to the turtle_control node."""
        goal_msg = Target.Goal()
        goal_msg.x = x
        goal_msg.y = y

        print(f"Sending goal: x={x}, y={y}")

        # TODO: send the goal to the action server

        while rclpy.ok() and not self.action_done_event.is_set():
            rclpy.spin_once(self)
        self.action_done_event.clear()

    def goal_response_callback(self, future):
        """Handle the response from the action server once the goal has been proccessed."""
        goal_handle = future.result()
        if not goal_handle.accepted:
            self.get_logger().info("Goal rejected :(")
            self.action_done_event.set()
            return

        self.get_logger().info("Goal accepted :)")

        get_result_future = goal_handle.get_result_async()
        get_result_future.add_done_callback(self.get_result_callback)

    def get_result_callback(self, future):
        """Handle the result from the action server once the goal has compleated."""
        result = future.result().result
        self.get_logger().info(f"Result success: {result.success}")
        self.action_done_event.set()

    # TODO: create a feedback callback


def main(args=None):
    """Run TurtlePlanner node."""
    rclpy.init(args=args)

    action_client = TurtlePlanner()

    action_client.send_goal(7.5, 5.5)
    # TODO: make the turtle do a square


if __name__ == "__main__":
    main()
