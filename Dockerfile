FROM ubuntu:jammy

# setup environment
SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND=noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get -q -y update && apt-get -q -y upgrade

# setup timezone
RUN echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    apt-get -q -y install --no-install-recommends tzdata

# install basic packages
RUN apt-get -q -y install --no-install-recommends \
    git \
    gnupg gnupg2 dirmngr \
    ca-certificates \
    software-properties-common coreutils \
    sudo wget curl unzip \
    lsb-release pkg-config \
    apt-utils \
    bash-completion \
    build-essential ccache ninja-build cmake \
    clang clang-format \
    gdb \
    python3 \
    python3-dev \
    python3-apt \
    python3-pip \
    iputils-ping iproute2 \
    neovim tmux

# setup sources.list
RUN echo "deb http://packages.ros.org/ros2/ubuntu jammy main" > /etc/apt/sources.list.d/ros2-latest.list

# setup keys (might change?)
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

# update apt repositories and install bootstrap tools
RUN apt-get -q -y update && apt-get -q -y install --no-install-recommends \
    python3-colcon-common-extensions \
    python3-colcon-mixin \
    python3-rosdep \
    python3-vcstool

# install python packages
RUN pip3 install -U \
    setuptools==58.2.0 \
    argcomplete \
    flake8 \
    flake8-builtins \
    flake8-comprehensions \
    flake8-deprecated \
    flake8-docstrings \
    flake8-quotes \
    pytest \
    pytest-repeat \
    pytest-rerunfailures \
    black

# install ros2
RUN apt-get -q -y install ros-humble-desktop

RUN echo -e "\nsource /opt/ros/humble/setup.bash" >> /root/.bashrc

RUN echo -e "\nsource /usr/share/colcon_argcomplete/hook/colcon-argcomplete.bash" >> /root/.bashrc

WORKDIR /onboarding_2022

CMD ["bash"]
